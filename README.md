A Python script to plot the signatures per minute of the
[Revoke Article 50 and remain in the EU.](https://petition.parliament.uk/petitions/241584)
petition.

Thanks (and appologies) to [@magicroundabout](https://twitter.com/magicroundabout)
for https://petition-track.uk/check-petition/241584/
