import warnings
from dataclasses import dataclass
from datetime import datetime, timedelta
from operator import attrgetter

import pandas as pd
from pandas import DataFrame, Series
import pytz
import requests
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from matplotlib.ticker import FuncFormatter
import seaborn as sns


@dataclass
class Prediction:
    time: pd.Timestamp
    predictor: np.polynomial.polynomial.Polynomial
    window: int


def convert_index_to_raw(index):
    """
    Turn a period index into something with minutes as a base
    The valueswill be relative to the most recent value in the sequence
    so all will be negative and the last entry should be zero
    """
    d = pd.to_numeric(index.values.astype('datetime64[m]'))
    return d - d.max()


def predict(df, last_count: int, target: int, windows, degree):
    """
    Given two windows over which to average, guess when the target will be reached.

    Args:
        df: the data
        last_count: the total number of signatures at last count
        target: the value to predict for
        windows: a list of number of hours over which to average
        degree: the degree of the polynomial to use
    """
    difference = target - last_count

    predictions = []
    for window in windows:
        recent_data = df["spm"][df.index >= (datetime.now(utc) - timedelta(hours=window))]
        raw_minutes = convert_index_to_raw(recent_data.index)

        # Find the point at which this ploynomial reaches the target
        try:
            fit = np.polynomial.polynomial.Polynomial.fit(raw_minutes, recent_data.values, deg=degree)
        except np.polynomial.polyutils.RankWarning:
            continue
        predicted_path = fit.integ(lbnd=0)
        try:
            positive_predictions = [root.real for root in (predicted_path - difference).roots() if root.real >= 0 and root.imag == 0]
        except np.linalg.LinAlgError:
            continue
        if not positive_predictions:
            continue
        predicted_mins = min(positive_predictions)

        # Check to see if the rate ever goes negative
        future_raw_minutes = np.linspace(raw_minutes[-1], predicted_mins)
        predicted_rates = fit(future_raw_minutes)
        if (predicted_rates < 0).any():
            continue

        # We're happy so let's convert the data and add it to the output
        predicted_delta = timedelta(minutes=predicted_mins)
        predicted_time = recent_data.index[-1] + predicted_delta
        predictions.append(Prediction(predicted_time, predicted_path, window))

    return sorted(predictions, key=attrgetter("time"))


def plot_prediction(predictions, target: int, start=None):
    """
    Plot the prediction graph

    Args:
        predictions: the set of target times for the prediction
        target: the value we're aiming for
        start: when to start the plot
    """

    # Calculate a regularised and scaled version of the count values

    count_resampled_M = df["count"].resample("1Min").mean().interpolate(method="cubic")

    # Get the information from the predictions

    now = count_resampled_M.index[-1]

    fast_prediction = predictions[0]
    slow_prediction = predictions[-1]

    fast_path_predictor = fast_prediction.predictor
    slow_path_predictor = slow_prediction.predictor

    current = count_resampled_M.at[now]
    target = target

    time_index = pd.period_range(start=now, end=slow_prediction.time, freq=count_resampled_M.index.freq)

    historical_fast_time_index = pd.period_range(start=now - pd.Timedelta(hours=fast_prediction.window), end=now, freq=count_resampled_M.index.freq)
    historical_slow_time_index = pd.period_range(start=now - pd.Timedelta(hours=slow_prediction.window), end=now, freq=count_resampled_M.index.freq)

    def to_raw_minutes(index):
        r = convert_index_to_raw(index.to_timestamp())
        return r - r.min()

    raw_minutes = to_raw_minutes(time_index)
    historical_fast_raw_minutes = to_raw_minutes(historical_fast_time_index)
    historical_fast_raw_minutes -= historical_fast_raw_minutes.max()
    historical_slow_raw_minutes = to_raw_minutes(historical_slow_time_index)
    historical_slow_raw_minutes -= historical_slow_raw_minutes.max()

    fast_path = fast_path_predictor(raw_minutes) + current
    slow_path = slow_path_predictor(raw_minutes) + current

    historical_fast_path = fast_path_predictor(historical_fast_raw_minutes) + current
    historical_slow_path = slow_path_predictor(historical_slow_raw_minutes) + current


    # run the prediction

    fast = Series(index=time_index, data=fast_path)
    slow = Series(index=time_index, data=slow_path)
    historical_fast = Series(index=historical_fast_time_index, data=historical_fast_path)
    historical_slow = Series(index=historical_slow_time_index, data=historical_slow_path)

    # We then plot the data

    prediction_fig = plt.figure(figsize=(8, 8/16*9), tight_layout=True)
    ax = prediction_fig.subplots(1)
    ax.set_title("Petition: Do not prorogue Parliament")

    count_resampled_M.loc[start:].tz_localize(None).plot(ax=ax)
    fast.plot(linestyle="--")
    slow.plot(linestyle="--", color=ax.get_lines()[1].get_color())
    ax.fill_between(time_index, fast_path, slow_path, color=ax.get_lines()[1].get_color(), alpha=0.3)
    historical_fast.plot(linestyle=":", color=ax.get_lines()[2].get_color())
    historical_slow.plot(linestyle=":", color=ax.get_lines()[2].get_color())
    ax.set_ylabel("Total signatures")
    ax.set_xlabel("")
    ax.set_ylim(bottom=-0.1, top=target)
    ax.tick_params("y", left=True, right=True, labelright=True)
    ax.yaxis.set_major_formatter(FuncFormatter(lambda x, _: f'{x/10**6} M'))
    ax.grid(True, which="major", axis="x")
    ax.legend(handles=[
        mlines.Line2D([], [], color=ax.get_lines()[0].get_color(), label='Signatures'),
        mlines.Line2D([], [], linestyle="--", color=ax.get_lines()[1].get_color(), label='Prediction'),
        ])
    return prediction_fig


sns.set()

warnings.filterwarnings("error")

tz = pytz.timezone('Europe/London')
utc = pytz.timezone('UTC')

# Get the data and clean up the DataFrame

petition_number = 269157
r = requests.get(f"https://petition-track.uk/api/v1/petition/{petition_number}/")
j = r.json()
#import json
#with open("data.json") as f:
#    j = json.load(f)
df = DataFrame(j)
df["data_timestamp"] = pd.to_datetime(df["data_timestamp"])
df = df.set_index("data_timestamp")
df.index = df.index.tz_localize(utc)
df.index = df.index.tz_convert(tz)
df = df.drop(["created_at", "id", "petition_id", "updated_at"], axis=1)

# Clean up the data by removing the duplicates caused by caching
# We keep the "first" since it's the oldest with the value
# and therefore closest to when it was reached

df = df.drop_duplicates(keep="first")

# Get the time between each real measurement so that we can calculate our rates correctly
# as well as how much the count has changed by

df["time_delta"] = df.index.to_series(keep_tz=True).diff()
df["count_delta"] = df["count"].diff()

# Calculate our "signatures per minute"

df["spm"] = df["count_delta"] / (df["time_delta"].dt.total_seconds() / 60)

# Start work on the predictions

# Let's calculate the time it will take to reach the next milestone
# We define this as the next time the most significant digit goes up by one
# So if we're on 3,654,567 then the target with be 4,000,000

low_hour_window = 2
high_hour_window = 6

last_count = df["count"].iloc[-1]
next_target = (int(str(last_count)[0])+1) * 10**(len(str(last_count))-1)

predictions_next = predict(df, last_count, next_target, range(low_hour_window, high_hour_window), 0)
f = plot_prediction(predictions_next, next_target, start='2019-03-20 06:00')
f.savefig("prediction.png")


# Now on to the signature rate graphs

five_min_resample = df["spm"].resample("5Min").mean().interpolate(method="pad")
hourly_mean = five_min_resample.rolling("60Min").mean()

fig = plt.figure(figsize=(8, 8/16*9), tight_layout=True)
ax = fig.subplots(1)
ax.set_title("Petition: Do not prorogue Parliament")
five_min_resample.tz_localize(None).plot(ax=ax, linewidth=0, marker='o', markersize=1)
hourly_mean.tz_localize(None).plot(ax=ax)
ax.set_ylabel("Signatures per minute")
ax.set_xlabel("")
ax.set_ylim(bottom=0)
ax.tick_params("y", left=True, right=True, labelright=True)
ax.grid(True, which="major", axis="x")
ax.legend(handles=[
    mlines.Line2D([], [], color=ax.get_lines()[0].get_color(), label='Signatures'),
    mlines.Line2D([], [], color=ax.get_lines()[1].get_color(), label='Averaged over an hour'),
    ])
plt.savefig("delta_all_time.png")

css = """
body {
    background-color: #ffffff;
    color: #22110a;
    line-height: 1.4;
    font-family: sans
}
h1 {
    font-size: 2.5em;
    text-align: center;
}
h2 {
    font-size:1.5em;
    padding: 0 0.5em;
    text-align: center;
}
h3 {
    font-size: 1em;
    padding: 0 0.5em;
    text-align: center;
}
p {
    font-family: sans-serif;
    text-align: center;
}
footer {
    background-color: #dcdcdc;
    padding: 0.2em 0.2em;
    font-size: small;
}
#content {
    text-align: justify;
    max-width: 60em;
    margin: 0 auto;
    padding: 0 1em;
}
"""

london_now = datetime.now(tz).strftime("%Y-%m-%d %H:%M")

with open("style.css", "w") as f:
    f.write(css)

html = f"""
<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title>PetitionStats: Do not prorogue Parliament</title>
  <link rel="stylesheet" type="text/css" href="style.css">

</head>

<body>
<div id="content">
  <h1>PetitionStats<br/>Do not prorogue Parliament</h1>

  <p>This page was last updated at {london_now} when the signature count was <strong>{last_count:,}</strong>.</p>

  <h2>Predictions</h2>
  <h3>Next milestone</h3>
  <p>By looking at the average signing rate over a range of windows (between {low_hour_window} hours and {high_hour_window} hours), we can try to predict a range of probabilities of when the petition will hit its next milestone.</p>
  <p>Using a linear fit, it is currently predicted to hit {next_target:,} between {predictions_next[0].time.strftime("%Y-%m-%d %H:%M")} and {predictions_next[-1].time.strftime("%Y-%m-%d %H:%M")}</p>
  <p><img src="prediction.png"/></p>

  <h2>Signature rates</h2>

  <h3>Since the beginning of the petition</h3>
  <p><img src="delta_all_time.png"/></p>

  <footer>
  <p>Graphs and analysis by <a href="https://twitter.com/milliams">Matt Williams</a>. Licensed as CC-BY-SA 4.0</p>
  <p>Historical data by <a href="https://twitter.com/magicroundabout">@magicroundabout</a></p>
  <p>Code available at <a href="https://gitlab.com/milliams/revokearticle50stats">GitLab</a></p>
  <p>Contains public sector information licensed under the <a href="https://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/">Open Government Licence v3.0.</a></p>
  </footer>
</div>
</body>
</html>
"""

with open("index.html", "w") as f:
    f.write(html)
